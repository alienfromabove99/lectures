```python tags=["initialize"]
from matplotlib import pyplot as plt 
import numpy as np
from math import pi
```
# Solutions for lecture 10 exercises

## Exercise 1: Equivalence of direct and reciprocal lattice

### Subquestion 1
$$
V^*=\left|\mathbf{b}_{1} \cdot\left(\mathbf{b}_{2} \times \mathbf{b}_{3}\right)\right| = \frac{2\pi}{V}\left| (\mathbf{a}_{2} \times \mathbf{a}_{3}) \cdot\left(\mathbf{b}_{2} \times \mathbf{b}_{3}\right)\right| = \frac{(2\pi)^3}{V}
$$

In the second equality, we used the reciprocal lattice vector definition (see notes). In the third equality, we used the identity:

$$
(\mathbf{a} \times \mathbf{b}) \cdot(\mathbf{c} \times \mathbf{d})=(\mathbf{a} \cdot \mathbf{c})(\mathbf{b} \cdot \mathbf{d})-(\mathbf{a} \cdot \mathbf{d})(\mathbf{b} \cdot \mathbf{c})
$$

### Subquestion 2
$$
\mathbf{a}_{i} \epsilon_{ijk} = \frac{2\pi}{V^*} (\mathbf{b}_{j} \times \mathbf{b}_{k})
$$

whereas $\epsilon_{ijk}$ is the [Levi-Civita tensor](https://en.wikipedia.org/wiki/Levi-Civita_symbol#Three_dimensions)

### Subquestion 3

BCC primitive lattice vectors are given by:
$$
\mathbf{a_1} = \frac{a}{2} \left(-\hat{\mathbf{x}}+\hat{\mathbf{y}}+\hat{\mathbf{z}} \right) \\
\mathbf{a_2} = \frac{a}{2} \left(\hat{\mathbf{x}}-\hat{\mathbf{y}}+\hat{\mathbf{z}} \right) \\
\mathbf{a_3} = \frac{a}{2} \left(\hat{\mathbf{x}}+\hat{\mathbf{y}}-\hat{\mathbf{z}} \right)
$$

using definition of reciprocal lattice vector (see notes), one can show:

$$
\mathbf{b_1} = \frac{2 \pi}{a} \left(\hat{\mathbf{y}}+\hat{\mathbf{z}} \right) \\
\mathbf{b_2} = \frac{2 \pi}{a} \left(\hat{\mathbf{x}}+\hat{\mathbf{z}} \right) \\
\mathbf{b_3} = \frac{2 \pi}{a} \left(\hat{\mathbf{x}}+\hat{\mathbf{y}} \right)
$$

which is FCC primitive lattice vectors. Using the result in Subquestion 2, the vice versa result is trivial

### Subquestion 4

Brillouin zone is most easily given by the Wigner Seitz unit cell (see notes) by constructing planes in the midpoint between a lattice point and a nearest neighbor. Our lattice is FCC, so the reciprocal lattice is BCC. Since BCC has 8 nearest neighbors (see interactive figure in last week's [Exercise 1: Diatomic crystal](https://solidstate.quantumtinkerer.tudelft.nl/9_crystal_structure/#exercise-1-diatomic-crystal)), there will be 8 planes. A polyhedron which has 8 faces is an octahedron.

## Exercise 2: Miller planes and reciprocal lattice vectors

### Subquestion 1

??? hint "First small hint"

    The $(hkl)$ plane intersects lattice at position vectors of $\frac{\mathbf{a_1}}{h}, \frac{\mathbf{a_2}}{k}, \frac{\mathbf{a_3}}{l}$. Can you define a general vector inside the $(hkl)$ plane? 
    
??? hint "Second small hint"

    Whats the best vector operation to show orthogonality between two vectors? 
    
### Subquestion 2

One can compute the normal to the plane by using result from Subquestion 1:

$\hat{\mathbf{n}} = \frac{\mathbf{G}}{|G|}$

For lattice planes, there is always a plane intersecting the zero lattice point (0,0,0). As such, the distance from this plane to the closest next one is given by:

$ d = \hat{\mathbf{n}} \cdot \frac{\mathbf{a_1}}{h} = \frac{2 \pi}{|G|} $

### Subquestion 3

Since $\rho=d / V$, we must maximize $d$. To do that, we must minimize $|G|$ (Subquestion 2). We must therefore use the smallest possible reciprocal lattice vector which means {100} family of planes (in terms of FCC primitive lattice vectors).

## Exercise 3: X-ray scattering in 2D

### Subquestion 1
```python
def reciprocal_lattice(N = 7, lim = 40):
    y = np.repeat(np.linspace(-18.4*(N//2),18.4*(N//2),N),N)
    x = np.tile(np.linspace(-13.4*(N//2),13.4*(N//2),N),N)

    plt.figure(figsize=(5,5))

    plt.plot(x,y,'o', markersize=10, markerfacecolor='none', color='k')
    plt.xlim([-lim,lim])
    plt.ylim([-lim,lim])
    plt.xlabel('$\mathbf{b_1}$')
    plt.ylabel('$\mathbf{b_2}$')
    plt.xticks(np.linspace(-lim,lim,5))
    plt.yticks(np.linspace(-lim,lim,5))
    
reciprocal_lattice()
plt.show()
```
### Subquestion 2

$k = \frac{2 \pi}{\lambda} = 37.9 nm^{-1}$

### Subquestion 3

Note that $|k| = |k'| = k $ since elastic scatering

```python
reciprocal_lattice()
# G vector
plt.arrow(0,0,13.4*2,18.4,color='r',zorder=10,head_width=2,length_includes_head=True)
plt.annotate('$\mathbf{G}$',(17,6.5),fontsize=14,ha='center',color='r')
# k vector
plt.arrow(-6,37.4,6,-37.4,color='b',zorder=11,head_width=2,length_includes_head=True)
plt.annotate('$\mathbf{k}$',(-8,18),fontsize=14, ha='center',color='b')
# k' vector
plt.arrow(-6,37.4,6+13.4*2,-37.4+18.4,color='k',zorder=11,head_width=2,length_includes_head=True)
plt.annotate('$\mathbf{k\'}$',(15,30),fontsize=14, ha='center',color='k')
plt.show()
```
## Exercise 4: Structure factors

### Subquestion 1
$S_\mathbf{G} = \sum_j f_j e^{i \mathbf{G} \cdot \mathbf{r_j}} = f(1 + e^{i \pi (h+k+l)}) =$

$$
\begin{cases}
      2f & \text{if $h+k+l$ is even}\\
      0 & \text{if $h+k+l$ is odd}
\end{cases}       
$$

where we used sum over the basis of BCC in $j$.

### Subquestion 2

See when $S_G$ is zero

### Subquestion 3
$S_\mathbf{G} =$
$$
\begin{cases}
      f_1 + f_2 & \text{if $h+k+l$ is even}\\
      f_1 - f_2 & \text{if $h+k+l$ is odd}
\end{cases}       
$$

### Subquestion 4

Due to bcc systematic absences, the peaks from lowest to largest angle are:
$(110),(200),(211), (220), (310)$

### Subquestion 5

$a = 2.9100 \unicode{xC5}$
