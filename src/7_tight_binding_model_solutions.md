```python tags=["initialize"]
import matplotlib
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()

pi = np.pi
```

# Solutions for lecture 7 exercises

## Exercise 1: Lattice vibrations

### Subquestion 1

Hint: Normal modes have the same function as $\mathbf{e}_1,\mathbf{e}_2,\mathbf{e}_3$ have in $\mathbb{R}^3$. That is, for every vector $\mathbf{v}$ we have $\mathbf{v} = a\mathbf{e}_1 + b\mathbf{e}_2 + c\mathbf{e}_3$.

Hint: The lecture concerns atom vibrations, so what will a phonon be?

??? hint "Major hint"

    What's the question's title?

Hint: What kind of particles obey Bose-Einstein statistics? What kind of 'particles' are phonons?

### Subquestion 2

Group velocity is given as $v=\hbar^{-1}\frac{\partial E}{\partial k}$ with $E=\hbar\omega$ and $g(\omega) = \frac{dN}{d\omega} = \frac{dN}{dk}\frac{dk}{d\omega}$. So we find: $$ v(k) = a\sqrt{\frac{\kappa}{m}}\cos(\frac{ka}{2})\frac{|k|}{k}$$ $$ g(\omega) = \frac{2L}{\pi}\frac{d}{d\omega} \bigg [\frac{2}{a}\sin^{-1}\bigg(\sqrt{\frac{m}{\kappa}}\frac{\omega}{2} \bigg) \bigg ] \\ = \frac{L}{\pi a} \frac{1}{\sqrt{\frac{4\kappa}{m}-\omega^2}}$$

### Subquestion 3

```python
pyplot.subplot(1,2,1)
k = np.linspace(-pi+0.01, pi-0.01, 300)
pyplot.plot(k[0:149], np.sin(k[0:149])/(np.sqrt(1-np.cos(k[0:149]))),'b');
pyplot.plot(k[150:300], np.sin(k[150:300])/(np.sqrt(1-np.cos(k[150:300]))),'b');
pyplot.xlabel(r'$ka$'); pyplot.ylabel('$v(k)$');
pyplot.xticks([-pi, 0, pi], [r'$-\pi/2$', 0, r'$\pi/2$']);
pyplot.yticks([-np.sqrt(2), 0, np.sqrt(2)], [r'$-2\sqrt{\frac{\kappa}{m}}$', 0, r'$2\sqrt{\frac{\kappa}{m}}$']);
pyplot.tight_layout();

pyplot.subplot(1,2,2)
w = np.linspace(-0.95, 0.95, 300);
g = 1/np.sqrt(1-w**2);
pyplot.plot(w, g, 'b');
pyplot.xlabel(r'$\omega$'); pyplot.ylabel('$g(w)$');
pyplot.xticks([-1, 0, 1], [r'$-2\sqrt{\frac{k}{m}}$', 0, r'$2\sqrt{\frac{k}{m}}$']);
pyplot.yticks([0.5, 1], [0, r'$\frac{L}{2\pi a}\sqrt{\frac{\kappa}{m}}$']);
pyplot.tight_layout();
```

### Subquestion 4

Hint: The group velocity is given as $v = \frac{d\omega}{dk}$, draw a coordinate system **under** or **above** the dispersion graph with $k$ on the x-axis in which you draw $\frac{d\omega}{dk}$. Draw a coordinate system **next** to the dispersion with *$g(\omega)$ on the y-axis* in which you graph $\big(\frac{d\omega}{dk}\big)^{-1}$.

??? hint "Plots"

    ![](figures/dispersion_groupv_dos.svg)

## Exercise 2: Vibrational heat capacity of a 1D monatomic chain

### Subquestion 1

For the energy we have: $$U = \int \hbar \omega g(\omega) (n(\omega,T) + \frac{1}{2})d\omega$$ with $g(\omega)$ as in Exercise 1 subquestion 2 and $n(\omega,T) = \frac{1}{e^{\hbar\omega/k_BT}-1}$.

### Subquestion 2

For the heat capacity we have: $$C = \frac{\partial U}{\partial T} = \int g(\omega) \hbar\omega \frac{\partial n(\omega,T)}{\partial T}d\omega$$

## Exercise 3: Next-nearest neighbors chain

### Subquestion 1

The Schrödinger equation is given as: $|\Psi\rangle = \sum_n \phi_n |n\rangle$ such that we find $$ E\phi_n = E_0\phi_n - t\phi_{n-1} - t\phi_{n+1} - t'\phi_{n-2} - t'\phi_{n+2}$$

### Subquestion 2

Solving the Schrödinger equation yields dispersion: $$E(k) = E_0 -2t\cos(ka) -2t'\cos(2ka)$$

### Subquestion 3

$$m_{eff} = \frac{\hbar^2}{2a^2}\frac{1}{2t\cos(ka)+4t'\cos(2ka)}$$

Plot for t=t':

```python
k1 = np.linspace(-pi, -pi/2-0.01, 300);
k2 = np.linspace(-pi/2+0.01, pi/2-0.01, 300);
k3 = np.linspace(pi/2+0.01, pi, 300);

pyplot.plot(k1, 1/(5*np.cos(k1)),'b');
pyplot.plot(k2, 1/(5*np.cos(k2)),'b');
pyplot.plot(k3, 1/(5*np.cos(k3)),'b');
pyplot.xlabel(r'$ka$'); pyplot.ylabel('$m_{eff}(k)$');
pyplot.xticks([-pi,0,pi],[r'$-\pi/a$',0,r'$\pi/a$']);
pyplot.yticks([],[]);
pyplot.tight_layout();
```

### Subquestion 4

Plots for 2t'=t, 4t'=t and 10t'=t:

```python
def m(k,t):
    return 1/(np.cos(k)+4*t*np.cos(2*k))
 
k1 = np.linspace(-1.6, -0.83, 300);
k2 = np.linspace(-0.826, 0.826, 300);
k3 = np.linspace(0.83, 1.6, 300);

pyplot.plot(k1, m(k1,2),'b');
pyplot.plot(k2, m(k2,2),'b');
pyplot.plot(k3, m(k3,2),'b',label='t=2t\'');
pyplot.xlabel('$k$'); pyplot.ylabel('$m_{eff}(k)$');
pyplot.xticks([-1.6,0,1.6],[r'$-\pi/a$',0,r'$\pi/a$']);
pyplot.yticks([0],[]);
pyplot.tight_layout();

k1 = np.linspace(-1.58, -0.81, 300);
k2 = np.linspace(-0.804, 0.804, 300);
k3 = np.linspace(0.81, 1.58, 300);

pyplot.plot(k1, m(k1,4),'r');
pyplot.plot(k2, m(k2,4),'r');
pyplot.plot(k3, m(k3,4),'r',label='t=4t\'');

k1 = np.linspace(-1.575, -0.798, 300);
k2 = np.linspace(-0.790, 0.790, 300);
k3 = np.linspace(0.798, 1.575, 300);

pyplot.plot(k1, m(k1,10),'k');
pyplot.plot(k2, m(k2,10),'k');
pyplot.plot(k3, m(k3,10),'k',label='t=10t\'');

pyplot.legend();
```


