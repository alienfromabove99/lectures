```python tags=["initialize"]
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()
```

_(based on chapter 3 of the book)_  

!!! success "Expected prior knowledge"

    Before the start of this lecture, you should be able to:

    - Write down the force acting on an electron in electric and magnetic fields
    - Solve Newton's equations of motion
    - Define concepts of voltage, electrical current, and conductivity

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Discuss the basics of Drude theory, which describes electron motion in metals.
    - Use Drude theory to analyze transport of electrons through conductors in electric and magnetic fields.
    - Describe concepts such as electron mobility and the Hall resistance.

## The starting point

The Ohm's law, familiar to most from the high school, states that voltage is proportional to current $V=IR$.
Since we are dealing with *material properties*, let us rewrite this into a relation that does not depend on the material geometry:
$$
V = I ρ \frac{l}{A} ⇒ E = ρ j,
$$
where $E≡V/l$ is the electric field, $ρ$ the material resistivity, and $j≡I/A$ the current through a unit cross-section.
Our goal is to understand how this law may arise microscopically, starting from reasonable (but definitely incomplete) assumptions.

- Electrons fly freely, and scatter at random uncorrelated times, with an average scattering time $τ$.
- After each scattering event, the electron's momentum randomizes with a zero average $⟨\mathbf{p}⟩=0$.
- The Lorentz force $\mathbf{F}_L=-e\left(\mathbf{E}+\mathbf{v}×\mathbf{B}\right)$ acts on the electrons.

The first assumption here is the least obvious: why the time between scattering events not depend on e.g. electron velocity?
Also observe that for now we forget that electrons also have fermionic statistics—this will come up in the next lecture, and turns out also helps to justify the first assumption.

Even under these minimal assumptions, our problem seems hard.
This is how electron motion looks like under these assumptions:

```python
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
from IPython.display import HTML

walkers = 20 # number of particles
tau = 1 # relaxation time
gamma = .3 # dissipation strength
a = 1 # acceleration
dt = .1 # infinitesimal
T = 20 # simulation time

v = np.zeros((2, int(T // dt), walkers), dtype=float) #

scattering_events = np.random.binomial(1, dt/tau, size=v.shape[1:])
angles = np.random.uniform(high=2*np.pi, size=scattering_events.shape) * scattering_events
rotations = np.array(
    [[np.cos(angles), np.sin(angles)],
     [-np.sin(angles), np.cos(angles)]]
)

for step in range(1, v.shape[1]):
    v[:, step] = v[:, step-1]
    v[0, step] += a * dt
    v[:, step] = np.einsum(
        'ijk,jk->ik',
        rotations[:, :, step-1, :],
        v[:, step, :]
    ) * (1 - gamma * scattering_events[step-1])

r = np.cumsum(v * dt, axis=1)

scattering_positions = np.copy(r)
scattering_positions[:, ~scattering_events.astype(bool)] = np.nan

fig = plt.figure()

scatter_pts = scattering_positions[:, :100]
trace = r[:, :100]

nz_scatters = tuple((np.hstack(scatter_pts[0])[~np.isnan(np.hstack(scatter_pts[0]))],
                    np.hstack(scatter_pts[1])[~np.isnan(np.hstack(scatter_pts[1]))]))

plt.axis([min(nz_scatters[0])-1,
          max(nz_scatters[0])+1,
          min(nz_scatters[1])-1,
          max(nz_scatters[1])+1])

lines = []
scatterers = []
for index in range(walkers):
    lobj = plt.plot([],[], lw=1, color='b', alpha=0.5)[0]
    lines.append(lobj)
    scatterers.append(plt.scatter([], [], s=10, c='r'))

def animate(i):
    for lnum, line in enumerate(lines):
        line.set_data(trace[0][:i, lnum], trace[1][:i, lnum])
        data = np.stack((scatter_pts[0][:i,lnum], scatter_pts[1][:i, lnum])).T
        scatterers[lnum].set_offsets(data)

anim = animation.FuncAnimation(fig, animate, interval=100)
plt.axis('off');
plt.close();

HTML(anim.to_html5_video())
```

Stop here for a second, and ask yourself how you would deal with this problem?

---

### Key idea

The answer follows from two important principles:

**Always write down what you want to compute.**

Our goal is finding the *electric current density* $j$.
Each electron with charge $e$ and velocity $\mathbf{v}$ carries current $e\mathbf{v}$.
Therefore if the electron density is $n$, the *average* current they carry is $ne⟨\mathbf{v}⟩$.
Our goal is then to compute the *average* velocity.

**Figuring out what happens on the average is simpler than figuring out what
happens with each individual element.**

Let us compute how the average velocity changes with time.
Consider the effect that scattering has over a small time $dt$.
A fraction $dt/τ$ of the electrons scatters, and that their average velocity becomes zero.
The rest of the electrons $(1 - dt/τ)$ are accelerated by the Lorentz force, and after $dt$ their velocity becomes
$$
m\mathbf{v}(t + dt) - m\mathbf{v}(t) = - e (\mathbf{E} + \mathbf{v} × \mathbf{B})⋅dt.
$$
Averaging the velocity of the two groups of particles, we get
$$
\begin{align}
m⟨\mathbf{v}(t+dt)⟩ &= [m⟨\mathbf{v}(t)⟩ - e (\mathbf{E} + \mathbf{v} × \mathbf{B})dt]\left(1 - \frac{dt}{\tau}\right) + 0⋅\frac{dt}{\tau}\\
                    &= m⟨\mathbf{v}(t)⟩ - dt [e (\mathbf{E} + \mathbf{v} × \mathbf{B}) - m⟨\mathbf{v}(t)⟩/τ] \\
                    &\quad\quad\quad\quad + e (\mathbf{E} + \mathbf{v} × \mathbf{B}) m⟨\mathbf{v}(t)⟩dt²/τ.
\end{align}
$$
We now neglect the term proportional to $dt²$ (it vanishes when $dt → 0$).
Finally, we recognize that $(⟨\mathbf{v}(t+dt)⟩ - (⟨\mathbf{v}(t)⟩)/dt = d⟨\mathbf{v}(t)⟩)/dt$, and arrive to
$$
m\frac{d⟨\mathbf{v}⟩}{dt} = -m\frac{⟨\mathbf{v}⟩}{τ} -e\left(\mathbf{E}+⟨\mathbf{v}⟩×\mathbf{B}\right).
$$
Observe that the first term on the right hand side has the same form as the viscous friction force—it tries to stop each particle at place.

We have now derived the necessary equation, the rest is merely applying it.

### Consequences of the Drude model

For convenience from now on we will omit the average signs, and write $\mathbf{v}$ instead of $⟨\mathbf{v}⟩$.
For a warm-up consider $\mathbf{B} = 0$, and a constant $\mathbf{E}$.
After we wait long enough, we expect the average electron velocity to become constant, $d\mathbf{v}/dt = 0$, and we immediately get
$$
\mathbf{v}=-\frac{eτ}{m}\mathbf{E}=-μ\mathbf{E},
$$
where we have defined the *mobility* $μ\equiv eτ/m$—the ratio between the electron *drift velocity* and the electric field.
Substituting this in the definition of the current density we arrive to
$$
\mathbf{j}=-en\mathbf{v}=\frac{n e^2τ}{m}\mathbf{E}=\sigma\mathbf{E},\quad \sigma=\frac{ne^2τ}{m}=ne\mu,
$$
where $\sigma$ is the conductivity, so that $ρ=\frac{1}{\sigma}$.

#### Origins of scattering

Something that Drude could not guess is that electrons do not scatter off of every atom (more on that in later weeks).
Instead scattering always happens due to some deviation from a perfect crystal:

- Phonons: $τ_\mathrm{ph}(T)$ ($τ_\mathrm{ph}\rightarrow\infty$ as $T\rightarrow 0$)
- Impurities/vacancies or other crystalline defects: $τ_0$

The scattering rates $1/τ$ due to different mechanisms add up:
$$
\frac{1}{τ}=\frac{1}{τ_\mathrm{ph}(T)}+\frac{1}{τ_0}\ \Rightarrow\ ρ=\frac{1}{\sigma}=\frac{m}{ne^2}\left( \frac{1}{τ_\mathrm{ph}(T)}+\frac{1}{τ_0} \right)\equiv ρ_\mathrm{ph}(T)+ρ_0
$$

This explains the empirical *Matthiessen's Rule* (1864).

![](figures/matthiessen.svg)

Here the solid is $ρ(T)$ of a pure crystal, and the dashed of an impure one.

What about the typical numbers—how fast do electrons travel through a copper wire?
Let's take $E = 1$ volt/m, $τ∼25$ fs (Cu, $T=300$ K).  
$⇒ v=\mu E=\frac{eτ}{m}E=2.5$ mm/s—extremely slow!

How come you don't wait for hours until the light turns on after you flick a switch?
This is an example failure of the Drude model, one which we will not have time to address.

### Hall effect

We now consider a conductive wire in a magnetic field $\mathbf{B}$ $⇒$ electrons are deflected in a direction perpendicular to $\mathbf{B}$ and $\mathbf{j}$ by the Lorentz force.

![](figures/hall_effect.svg)

$\mathbf{E}_\mathrm{H}$ is the electric field caused by the Lorentz force, leading to a _Hall voltage_ in the direction perpendicular to $\mathbf{B}$ and $\mathbf{j}$.

Once again, we consider the steady state, $d\mathbf{v}/dt = 0$.
After substituting $\mathbf{v} = \mathbf{j}/ne$, we arrive to
$$
\mathbf{E}=\frac{m}{ne^2τ}\mathbf{j} + \frac{1}{ne}\mathbf{j}\times\mathbf{B}.
$$
The first term is the same as before, while the second is the electric field **perpendicular** to the current flow.
In other words, if we send a current through a sample and apply a magnetic field, a voltage develops in the direction perpendicular to the current—this is called *Hall effect*, the voltage is called *Hall voltage*, and the proportionality coefficient $B/ne$ the *Hall resistivity*.

The above relation is convenient to write in a matrix form
$$
E_a = ∑_b ρ_{ab} j_b,
$$
with $a, b ∈ \{x, y, z\}$, and $ρ$ the *resistivity matrix*.
Its diagonal elements are $ρ_{xx}=ρ_{yy}=ρ_{zz}=m/ne^2τ$—the same as without magnetic field.
The only nonzero off-diagonal elements when $\mathbf{B}$ points in the $z$-direction are
$$
ρ_{xy}=-ρ_{yx}=\frac{B}{ne}\equiv -R_\mathrm{H}B,
$$
where $R_H=-1/ne$ is the *Hall coefficient*.
So by measuring the Hall voltage and knowing the electron charge, we can determine the density of free electrons in a material.

While most materials have $R_\mathrm{H}<0$, interestingly some materials are found to have $R_\mathrm{H}>0$. This would imply that the charge of the carriers is positive. We will see later in the course how to interpret this.

## Conclusions

1. Drude theory is a microscopic justification of the Ohm's law. Resistivity is caused by electrons that scatter with some characteristic time $τ$.
2. The Lorentz force leads to the Hall voltage that is perpendicular to the direction of electric current pushed through a material.

## Exercises

### Warm-up questions

1. How does the resistance of a purely 2D material depend on its size?
2. Check that the units of mobility and the Hall coefficient are correct.  
   (As you should always do!)
3. Explain why the scattering rates due to different types of scattering events add up.

### Exercise 1: Extracting quantities from basic Hall measurements
We apply a magnetic field $\bf B$ perpendicular to a planar (two-dimensional) sample that sits in the $xy$ plane. The sample has width $W$ in the $y$-direction, length $L$ in the $x$-direction and we apply a current $I$ along $x$.

  1. Suppose we measure a Hall voltage $V_H$. Express the Hall resistance $R_{xy} = V_H/I$ as a function of magnetic field. Does $R_{xy}$ depend on the geometry of the sample? Also express $R_{xy}$ in terms of the Hall coefficient $R_H$.
  2. Assuming we know the charge density $n$ in the sample, what quantity can we extract from a measurement of the Hall resistance? Would a large or a small electron density give a Hall voltage that is easier to measure?
  3. Express the longitudinal resistance $R=V/I$, where $V$ is the voltage difference over the sample along the $x$ direction, in terms of the longitudinal resistivity $ρ_{xx}$. Suppose we extracted $n$ from a measurement of the Hall resistance, what quantity can we extract from a measurement of the longitudinal resistance? Does the result depend on the geometry of the sample?

### Exercise 2: Motion of an electron in a magnetic and an electric field.
We first consider an electron in free space, moving in a plane perpendicular to a magnetic field $\mathbf{B}$ with velocity $\mathbf{v}$.

  1. Write down the Newton's equation of motion for the electron, compute $\frac{d\mathbf{v}}{{dt}}$.
  2. What is the shape of the motion of the electron? Calculate the characteristic frequency and time-period $T_c$ of this motion for $B=1$ Tesla.
  3. Now we accelerate the electron by adding an electric field $\mathbf{E}$ that is perpendicular to $\mathbf{B}$. Adjust the differential equation for $\frac{d\mathbf{v}}{{dt}}$ found in (1) to include $\mathbf{E}$. Sketch the motion of the electron.
  4. We now consider an electron in a metal. Include the Drude scattering time $τ$ into the differential equation for the velocity you formulated in 4.


### Exercise 3: Temperature dependence of resistance in the Drude model
   We consider copper, which has a density of 8960 kg/m$^3$, an atomic weight of 63.55 g/mol, and a room-temperature resistivity of $ρ=1.68\cdot 10^{-8}$ $\Omega$m. Each copper atom provides one free electron.

  1. Calculate the Drude scattering time $τ$ at room temperature.
  2. Assuming that electrons move with the thermal velocity $\langle v \rangle = \sqrt{\frac{8k_BT}{\pi m}}$, calculate the electron mean free path $\lambda$.
  3. The Drude model assumes that $\lambda$ is independent of temperature. How does the electrical resistivity $ρ$ depend on temperature under this assumption? Sketch $ρ(T)$.
  5. Compare your sketch of $ρ(T)$ with that in the lecture notes. In what respect do they differ? Discuss possible reasons for differences.

### Exercise 4: The Hall conductivity matrix and the Hall coefficient
We apply a magnetic field $\bf B$ perpendicular to a current carrying 2D sample in the xy plane. In this situation, the electric field $\mathbf{E}$ is related to the current density $\mathbf{J}$ by the resistivity matrix:

$$\mathbf{E} = \begin{pmatrix} ρ_{xx} & ρ_{xy} \\ ρ_{yx} & ρ_{yy} \end{pmatrix} \mathbf{J}$$

  1. Sketch $ρ_{xx}$ and $ρ_{xy}$ as a function of the magnetic field $\bf B$.
  2. Invert the resistivity matrix to obtain the conductivity matrix $$\begin{pmatrix} \sigma_{xx} & \sigma_{xy} \\ \sigma_{yx} & \sigma_{yy} \end{pmatrix} $$, allowing you to express $\mathbf{J}$ as a function of $\mathbf{E}$.
  3. Sketch $\sigma_{xx}$ and $\sigma_{xy}$ as a function of the magnetic field $\bf B$.
  4. Give the definition of the Hall coefficient. What does the sign of the Hall coefficient indicate?
