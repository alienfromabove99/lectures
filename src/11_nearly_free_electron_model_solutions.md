```python tags=["initialize"]
from matplotlib import pyplot as plt 
import numpy as np
from math import pi
```
$$\newcommand{\braket}[1]{\left\langle{#1}\right\rangle}$$
$$\newcommand{\bra}[1]{\left\langle{#1}\right|}$$
$$\newcommand{\ket}[1]{\left|{#1}\right\rangle}$$

# Solutions for lecture 11 exercises

## Exercise 1: Bloch's model

### Subquestion 1

It must obey the crystal symmetry, such as the translational symmetry of the lattice described by the lattice vectors $\mathbf{a}_1, \mathbf{a}_2$ and $\mathbf{a}_3$.

### Subquestion 2
From the periodicity of the wavefunction with the real space lattice vectors, it follows that the kinetic part of the Hamiltonian, here denoted $\hat{K}$, will commute with the translation operator $\hat{T}_{\alpha, \beta, \gamma}$. 
\begin{align*}
[\hat{T}_{\alpha,\beta,\gamma},\hat{H}]&= \left(V(\mathbf{r}-\alpha \mathbf{a}_1-\beta \mathbf{a}_2 - \gamma \mathbf{a}_3)-V(\mathbf{r})\right)\hat{T}_{\alpha,\beta,\gamma}=0\\
\end{align*}
Where we used that the lattice potential is periodic with integer lattice constants.

### Subquestion 3
\begin{align*}
\hat{T}_{\alpha,\beta,\gamma}u_n(\mathbf{r})e^{i\mathbf{k}\cdot\mathbf{r}}&=e^{-i\mathbf{k}\cdot (\alpha \mathbf{a}_1+\beta \mathbf{a}_2 + \gamma \mathbf{a}_3)} u_n(\mathbf{r})e^{i\mathbf{k}\cdot \mathbf{r}}\\
\end{align*}
The eigenfunctions of $\hat{H}$ must also be Bloch waves.

### Subquestion 4
\begin{equation*}
\nabla^2\psi_n(\mathbf{r})=e^{i\mathbf{k}\cdot\mathbf{r}}\left[ \nabla^2 u_n(\mathbf{r})-k^2u_n(\mathbf{r})+2i\mathbf{k}\nabla u_n(\mathbf{r}) \right].
\end{equation*}
Once this is explicitly written in the Schr. eqn, the complex exponentials cancel out. 

### Subquestion 5
$u_n(\mathbf{r})$ becomes a normalization constant that is independent of position. Hence, the momentum operators return zero, and the only term that remains is $\hbar^2k^2/2m$ (which is indeed the free electron dispersion). 

##  Exercise 2: The Central Equation in 1D

### Subquestion 1
All $k_n$ that differ by an integer multiple of $2\pi/a$ from $k_0$ have the exact same wavefunction.

### Subquestion 2

\begin{equation*}
\phi_n(x)=\frac{1}{\sqrt{\Omega}} \exp\left[i \left(k_0+\frac{2\pi n}{a}\right)x \right]
\end{equation*}
\begin{equation}
E_n=\frac{\hbar^2}{2m}\left(k_0+\frac{2\pi n}{a}\right)^2
\end{equation}

### Subquestion 3
```python
def dispersions(N = 5):

    x0 = np.linspace(-N*2*pi,N*2*pi,2*N+1)
    x = np.tile(np.linspace(-N*pi,N*pi,500),(2*N+1,1))

    y = []
    for i, offset in enumerate(x0):
        y.append((x[i]-offset)**2)
    
    plt.figure(figsize=(5,5))
    plt.axvspan(-pi, pi, alpha=0.2, color='red')
    
    for i in range(2,9):
        plt.plot(x[i],y[i])
        plt.axvline(1+x0[i],0,1,color='k')    

    plt.text(1.2-2*pi,37,'$k_{-1}$',fontsize=16)
    plt.text(1.2,37,'$k_0$',fontsize=16)
    plt.text(1.2+2*pi,37,'$k_1$',fontsize=16)
    plt.text(-2,59,'1st BZ',fontsize=19)
    
    plt.axhline(0.8,0,1,linestyle='dashed')
    plt.axhline(28,0,1,linestyle='dashed')
    plt.axhline(53,0,1,linestyle='dashed')
    
    plt.xlim([-3*pi,3*pi])
    plt.ylim([0,70])
    plt.xlabel('$k$',fontsize=19)
    plt.ylabel('$E$',fontsize=19)
    plt.xticks((-2*pi, -pi, 0 , pi,2*pi),('$-2\pi/a$','$-\pi/a$','$0$','$\pi/a$','$2\pi/a$'),fontsize=15)
    plt.yticks((1,27,54),('$E_0$','$E_1$','$E_{-1}$'))
    
dispersions(5)
```
### Subquestion 4

First the kinetic term,
\begin{equation}
\braket{\phi_m|\hat{K}|\phi_n}=C_m\frac{\hbar^2k_m^2}{2m}
\end{equation}
And the potential term,
\begin{align}
\braket{\phi_m|V(x)|\psi}=\sum_{n=-\infty}^{\infty}C_n\int_0^a V(x) e^{-i\frac{2\pi}{a}(m-n)x}dx
\end{align}
then relabel indices and combine both expressions to find the final answer and expression for $\varepsilon_m$ (which is the free electron dispersion).

### Subquestion 5
From the expression for the energy, it is clear that the difference with respect to the free electron model is given by the Fourier component $V_{m-n}$, describing the coupling between two states $m$ and $n$. The question becomes: when does this term contribute significantly? For that we look at two orthogonal states $\phi_n$ and $\phi_m$, and construct the Hamiltonian in the basis ($\phi_n$,$\phi_m$),  
\begin{equation}
\hat{H}=
\begin{pmatrix}
\dfrac{\hbar^2 k_n^2}{2m} & V_{n-m}\\
V_{m-n} & \dfrac{\hbar^2 k_m^2}{2m}
\end{pmatrix}
\end{equation}
The eigenvalues of this fellow are 
\begin{equation}
E=\frac{\hbar^2(k_n^2+k_m^2)}{4m}\pm\sqrt{\frac{\hbar^4}{16m^2}(k_n^2-k_m^2)+|V_{n-m}|^2}.
\end{equation}
This clearly displays that only if $|k_n|\approx |k_m|$, the band structure will be affected (given that the potential is weak, and therefore small). This nicely demonstrates how an avoided crossing arises.

## Exercise 3: The Tight Binding Model versus the Nearly Free Electron Model

### Subquestion 1
We construct the Hamiltonian (note that we have exactly one delta-peak per unit cell of the lattice),
\begin{equation*}
\hat{H}=
\begin{pmatrix}
\braket{\psi_1|\hat{H}|\psi_1} & \braket{\psi_1|\hat{H}|\psi_2}\\
\braket{\psi_2|\hat{H}|\psi_1} & \braket{\psi_2|\hat{H}|\psi_2}
\end{pmatrix}
\end{equation*}
The bottom band means we have to pick the lowest energy band, i.e. the dispersion with the lowest eigenvalues, which is
\begin{equation*}
E_-(k) = -\frac{\lambda}{a}+\frac{\hbar^2}{4m}\left[k^2+\left(k-\frac{2\pi}{a}\right)^2 \right]-\sqrt{\left(\frac{\hbar^2}{4m}\left[k^2-\left(k-\frac{2\pi}{a}\right)^2 \right]\right)^2 + \left(\frac{\lambda}{a}\right)^2}
\end{equation*}

### Subquestion 2
See the lecture notes!

### Subquestion 3

\begin{equation}
\varepsilon_0=\braket{n|\hat{H}|n}=\braket{n|\hat{K}|n}+\braket{n|\hat{V}(x)|n}
\end{equation}
We first consider the kinetic part
\begin{equation}
\braket{n|\hat{K}|n}=\frac{\hbar^2\kappa^2}{2m},
\end{equation}
where $\kappa=m\lambda/\hbar^2$. Then we turn to the potential term
\begin{align*}
\braket{n|\hat{V}(x)|n}&=-\lambda \kappa \int_{-\infty}^{\infty}e^{-\kappa|x|}\sum_n\delta(x+na) e^{-\kappa|x|}dx \approx -\lambda \kappa,
\end{align*}
where we have applied the limit $\lambda a\gg \hbar^2/m$. The coupling between two consecutive delta peaks is best treated in the same way,
\begin{equation}
\braket{n-1|\hat{K}|n}= \frac{\hbar^2 \kappa^3}{2m}\left( \frac{1}{\kappa}-a\right)e^{-\kappa a}
\end{equation}
and
\begin{equation}
\braket{n-1|\hat{V}|n}=-2\lambda \kappa \frac{e^{-\kappa a}}{1-e^{-2\kappa a}}.
\end{equation}

### Subquestion 4

| .. | Lower Band minimum | Lower Band Width|
| --- | --- | --- |
| TB model | $\varepsilon_0-2t$ | $4t$|
| NFE model | $E_-(0)$ | $E_{-}(\pi/2)-E_-(0)$ |


### Subquestion 5
Notice which approximations were made! For large $\lambda a$, the tight binding is more accurate, while for small $\lambda a$, the nearly free electron model is more accurate. The transition point for the regimes lies around $\lambda a\approx \hbar^2/m$. 
